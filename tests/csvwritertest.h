#ifndef CSVWRITERTEST_H
#define CSVWRITERTEST_H

#include <QObject>
#include <QtTest/QtTest>

class CsvWriterTest : public QObject
{
    Q_OBJECT
public:
    explicit CsvWriterTest(QObject *parent = 0);

private slots:
    void testQuote();
    void testWriteHeader();
    void testWriteRow();
};

#endif // CSVWRITERTEST_H
