#include "inputparsertest.h"
#include "inputparser.h"

InputParserTest::InputParserTest(QObject *parent) :
    QObject(parent)
{
}

void InputParserTest::decodeLine()
{
    QTextStream stream("a;b;c;\"bonjour\";tricky \";string with \"\" and \n inside\";yes");
    QStringList expected;
    expected << "a" << "b" << "c" << "bonjour"
             << "tricky ;string with \" and \n inside" << "yes";

    InputParser parser(&stream);
    QStringList result = parser.decodeLine();

    QCOMPARE(result, expected);
}

void InputParserTest::next()
{
    QTextStream stream("a;b;c\n1;2;3\n4;5;6");
    QMap<QString, QString> expected1, expected2, result;

    expected1["a"] = "1";
    expected1["b"] = "2";
    expected1["c"] = "3";

    expected2["a"] = "4";
    expected2["b"] = "5";
    expected2["c"] = "6";

    InputParser parser(&stream);

    result = parser.next();
    QCOMPARE(result, expected1);

    result = parser.next();
    QCOMPARE(result, expected2);
}
