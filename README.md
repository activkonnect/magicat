MagiCat
=======

This project helps you to automatically fill a form. What you need is a CSV
file as input plus a JS script that fills the form out in your stead.

Default configuration and bundled script is tailored to fill inscription forms
for LunivR.

## Mac OS X Packaging

### Prerequisites

- A developer ID certificate from Apple 
  (https://developer.apple.com/membercenter/)
- `dmgbuild` installed (https://pypi.python.org/pypi/dmgbuild)

### Compiling

Aside from compiling the app with Qt Creator, you need to run

```
/Users/mainuser/Qt/5.5/clang_64/bin/macdeployqt app/magicat.app -codesign=~/Downloads/developerID_application.cer
/usr/local/bin/dmgbuild -s dmgbuild_cfg.py -D app=build/release/app/MagiCat.app "MagiCat" magicat.dmg
```