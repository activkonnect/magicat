#ifndef INPUTPARSER_H
#define INPUTPARSER_H

#include <QObject>
#include <QTextStream>
#include <QMap>
#include <QString>
#include <QStringList>
#include <QChar>

class InputParser : public QObject
{
    Q_OBJECT
public:
    explicit InputParser(QTextStream *input, QObject *parent = 0);
    QStringList decodeLine();
    QMap<QString, QString> next();

private:
    QTextStream *input;
    QStringList columns;
    static const QChar separator;
    static const QChar quote;
};

#endif // INPUTPARSER_H
