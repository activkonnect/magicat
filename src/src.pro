# ---
# Magicat lib sub-project
# ---

include(../defaults.pri)

# Basic declaration
TARGET = magicat
TEMPLATE = lib
CONFIG += c++11
CONFIG += staticlib
QMAKE_MAC_SDK = macosx10.11

# Sources
SOURCES += inputparser.cpp \
    manager.cpp \
    dataset.cpp \
    mapmodel.cpp \
    magicatjs.cpp \
    csvwriter.cpp
HEADERS += inputparser.h \
    manager.h \
    dataset.h \
    mapmodel.h \
    magicatjs.h \
    csvwriter.h
