#ifndef CSVWRITER_H
#define CSVWRITER_H

#include <QObject>
#include <QStringList>
#include <QList>
#include <QMap>
#include <QTextStream>

class CsvWriter : public QObject
{
    Q_OBJECT
public:
    explicit CsvWriter(QTextStream *stream, QStringList columns, QObject *parent = 0);

    void writeHeader();
    void writeRow(QMap<QString, QString> *row);
    static QString quote(QString str);

private:
    QStringList columns;
    QTextStream *stream;
};

#endif // CSVWRITER_H
