#ifndef MANAGER_H
#define MANAGER_H

#include <QObject>
#include <QFileInfo>
#include <QMap>
#include <QVector>
#include <QSet>
#include <QSettings>
#include <QVariant>
#include <QString>

#include "dataset.h"

class Manager : public QObject
{
    Q_OBJECT
public:
    explicit Manager(QObject *parent = 0);

signals:
    void changedDataset(Dataset *dataset);
    void done();
    void progress(int progress);

public slots:
    void load(QString filename);
    void markDone(Dataset *dataset);
    void sendDataset();

private:
    QVector<QMap<QString, QString> > data;
    QSet<int> todo;
    QSettings settings;
    QString filename;
    QString generatedSettingsKey;

    void todoFromQVAriant(QVariant variantTodo);
    QVariant todoToQVariant();
    QString settingsKey(QString filename);
    void emitProgress();
};

#endif // MANAGER_H
