#include "csvwriter.h"

CsvWriter::CsvWriter(QTextStream *stream, QStringList columns, QObject *parent) :
    QObject(parent),
    columns(columns),
    stream(stream)
{
    qSort(this->columns);
}

void CsvWriter::writeHeader()
{
    *stream << columns.join(";") << endl;
}

void CsvWriter::writeRow(QMap<QString, QString> *row)
{
    QStringList rowContent;

    foreach (QString column, columns) {
        rowContent << quote((*row)[column]);
    }

    *stream << rowContent.join(";") << endl;
}

QString CsvWriter::quote(QString str)
{
    bool shouldQuote = str.contains("\"");

    str.replace("\r\n", "\\n");
    str.replace("\n", "\\n");
    str.replace("\r", "");

    if (shouldQuote) {
        str.replace("\"", "\"\"");
        str = "\"" + str + "\"";
    }

    return str;
}
