#include "fillwindow.h"
#include "ui_fillwindow.h"

#include <QUrl>
#include <QRegExp>
#include <QWebView>
#include <QWebFrame>
#include <QFileDialog>
#include <QInputDialog>
#include <QMessageBox>
#include <QDebug>
#include <QVariantMap>
#include <QVariant>
#include <QMapIterator>
#include <QSettings>
#include <QTimer>
#include <QStandardItemModel>
#include <QIcon>

#include "manager.h"
#include "mapmodel.h"
#include "magicatjs.h"
#include "csvwriter.h"

FillWindow::FillWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::FillWindow),
    currentDataset(0),
    magicatJS(new MagicatJS(this))
{
    // --
    // UI
    // --

    ui->setupUi(this);
    QWebSettings::globalSettings()->setAttribute(QWebSettings::DeveloperExtrasEnabled, true);

    QIcon icon(":/icons/icon.png");
    setWindowIcon(icon);
    setWindowIconText("MagiCat");

    // --
    // Model
    // --

    ui->userData->setModel(&mapModel);

    // --
    // Signals connection
    // --

    // URL changing shit
    connect(ui->userURL, SIGNAL(returnPressed()),
            ui->actionLoadUserURL, SLOT(trigger()));
    connect(ui->goToURL, SIGNAL(clicked()),
            ui->actionLoadUserURL, SLOT(trigger()));

    // Data related
    connect(&manager, SIGNAL(changedDataset(Dataset*)),
            this, SLOT(updateDataset(Dataset*)));
    connect(&manager, SIGNAL(progress(int)), ui->workCompletion, SLOT(setValue(int)));
    connect(&manager, SIGNAL(done()), this, SLOT(sayDone()));
    connect(magicatJS, SIGNAL(finished()), this, SLOT(finishEntry()));
    connect(magicatJS, SIGNAL(failed(QString)), this, SLOT(reportFail(QString)));

    // --
    // Settings restoration
    // --

    // Data file
    dataFile = settings.value("defaults/data_file").toString();
    loadData(dataFile);

    // Init URL
    initUrl = settings.value(
        "defaults/init_url",
        "https://apps.univ-lr.fr/cgi-bin/WebObjects/RA.woa?passDlg=9Qt9WgNxVg4M68z2&forceProfil=E"
    ).toString();
    setUrl(initUrl);

    // Load script
    scriptName = settings.value("defaults/script_name", ":/script/script.js").toString();
    loadScript(scriptName);

    // Fail log
    failLogFile = settings.value("defaults/fail_log").toString();
}

FillWindow::~FillWindow()
{
    delete ui;
}

void FillWindow::updateDataset(Dataset *dataset)
{
    currentDataset = dataset;
    mapModel.update(dataset->getData());
}

void FillWindow::injectJS()
{
    QWebFrame *frame = ui->webView->page()->mainFrame();

    magicatJS->updateData(mapModel.getMapData());

    if (currentDataset && !currentDataset->getData()->empty()) {
        frame->addToJavaScriptWindowObject(QString("$$magicat"), magicatJS);
        frame->evaluateJavaScript(script);
    }
}

void FillWindow::setUrl(QString url)
{
    ui->userURL->setText(url);
    on_actionLoadUserURL_triggered();
}

void FillWindow::loadScript(QString filename)
{
    if (!filename.isEmpty()) {
        QFile file(filename);

        if (file.open(QFile::ReadOnly | QFile::Text)) {
            QTextStream in(&file);
            script = in.readAll();
        }
    }
}

void FillWindow::loadData(QString filename)
{
    if (!filename.isEmpty()) {
        manager.load(filename);
    }
}

void FillWindow::finishEntry()
{
    manager.markDone(currentDataset);
    setUrl(initUrl);
}

void FillWindow::promptFailLogFile()
{
    QString newFailLogFile;

    while (newFailLogFile.isEmpty()) {
        newFailLogFile = QFileDialog::getSaveFileName(
            this,
            tr("Choose a failure log file"),
            QDir::homePath(),
            tr("CSV Files (*.csv)"),
            NULL,
            QFileDialog::Options(QFileDialog::DontConfirmOverwrite)
        );
    }

    failLogFile = newFailLogFile;
    settings.setValue("defaults/fail_log", failLogFile);
}

void FillWindow::reportFail(QString logEntry)
{
    while (failLogFile.isEmpty()) {
        promptFailLogFile();
    }

    QFile f(failLogFile);

    if (!f.open(QFile::Append | QFile::Text)) {
        failLogFile = "";
        reportFail(logEntry);
    }

    QStringList fields = currentDataset->getData()->keys();
    fields << "$$reject_reason";

    QMap<QString, QString> map(*currentDataset->getData());
    map["$$reject_reason"] = logEntry;

    QTextStream out(&f);
    CsvWriter writer(&out, fields);

    if (f.size() == 0) {
        writer.writeHeader();
    }

    writer.writeRow(&map);
    finishEntry();
}

void FillWindow::sayDone()
{
    QMessageBox::information(this, tr("Fini"), tr("C'est tout fini !"), QMessageBox::Ok);
    QTimer::singleShot(100, this, SLOT(on_actionCat_triggered()));
}

void FillWindow::on_actionLoadUserURL_triggered()
{
    QString url = ui->userURL->text();
    QRegExp rx("^(\\w+://|qrc:/).*");

    if (!rx.exactMatch(url)) {
        ui->userURL->setText("http://" + url);
    }

    ui->webView->setUrl(QUrl(ui->userURL->text()));
}

void FillWindow::on_webView_urlChanged(const QUrl &url)
{
    ui->userURL->setText(url.toString());
}

void FillWindow::on_actionLoad_triggered()
{
    dataFile = QFileDialog::getOpenFileName(this,
                                            tr("Load"),
                                            QDir::homePath(),
                                            tr("CSV Files (*.csv)"),
                                            NULL,
                                            QFileDialog::Options(QFileDialog::ReadOnly));

    loadData(dataFile);
    settings.setValue("defaults/data_file", dataFile);
}

void FillWindow::on_actionLoad_script_triggered()
{
    scriptName = QFileDialog::getOpenFileName(this,
                                              tr("Load"),
                                              QDir::homePath(),
                                              tr("Javascript (*.js)"),
                                              NULL,
                                              QFileDialog::Options(QFileDialog::ReadOnly));

    if (!scriptName.isEmpty()) {
        loadScript(scriptName);
        settings.setValue("defaults/script_name", scriptName);
    }
}

void FillWindow::on_actionQuit_triggered()
{
    QApplication::quit();
}

void FillWindow::on_webView_loadFinished(bool ok)
{
    if (ok) {
        injectJS();
    }
}

void FillWindow::on_actionSet_Initial_URL_triggered()
{
    bool ok;
    QString url = QInputDialog::getText(this, tr("Choose an initial URL"), tr("Initial URL:"), QLineEdit::Normal, initUrl, &ok);

    if (ok && !url.isEmpty()) {
        settings.setValue("defaults/init_url", url);
        setUrl(url);
    }
}

void FillWindow::on_resetButton_clicked()
{
    setUrl(initUrl);
}

void FillWindow::on_validateButton_clicked()
{
    finishEntry();
}

void FillWindow::on_actionSet_fail_log_location_triggered()
{
    promptFailLogFile();
}

void FillWindow::on_failButton_clicked()
{
    reportFail("Manual reject");
}

void FillWindow::on_actionCat_triggered()
{
    setUrl("qrc:/cat/cat.html");
}
